var url = {
      'Blog': "http://traineediarysb.blogspot.com/feeds/posts/default",
      'Lifehacker': "http://lifehacker.com/vip.xml",
      'Porn': "http://www.pornxxvideos.com/rss_videos.php?id=3",
      'TechCrunch': "http://feeds.feedburner.com/TechCrunch/",
      'Quora': "http://www.quora.com/rss",
      'News':  "http://feeds.huffingtonpost.com/huffingtonpost/raw_feed",
      'Hsblog': "http://feeds.feedburner.com/HighScalability?format=xml",
      'codeofhonor': "http://www.codeofhonor.com/blog/feed"
};
var channel;
var total_page = 0;
var load_modernizer = function () {
	var $container 	= $( '#flip' ),
	$pages		= $container.children().hide();
		Modernizr.load({
		test: Modernizr.csstransforms3d && Modernizr.csstransitions,
		yep : ['js/jquery.tmpl.min.js','js/jquery.history.js','js/core.string.js','js/jquery.touchSwipe-1.2.5.js','js/jquery.flips.js'],
		nope: 'css/fallback.css',
		callback : function( url, result, key ) {
			
			if( url === 'css/fallback.css' ) {
				$pages.show();
			}
			else if( url === 'js/jquery.flips.js' ) {
				$container.flips();
			}
		},
		complete: function () {alert("done");}
	});
};

var add_page = function (i) {
	var app = document.getElementById("flip");
	var title = document.createElement("div");
	title.className = "f-title";
	var newpage = document.createElement("div");
	newpage.id = "feed" + Math.floor(i/4);
	newpage.className = "f-page";
	var header = document.createElement("div");
	header.className = 'f-title';
	var heading = document.createElement("h3");
	heading.appendChild(document.createTextNode(channel));
	header.appendChild(heading);
  var home = document.createElement("a");
  home.href = "index.html";
  home.setAttribute('href',"index.html");
  home.appendChild(document.createTextNode("Home"));
  title.appendChild(home);
	//console.log(newpage.id);
	title.appendChild(header);

  newpage.appendChild(title);
	app.appendChild(newpage);
      total_page += 1;
};

var handle_article = function(result, i) {
      var entry = result.feed.entries[i];
      var div = document.createElement("div");
      div.className = 'article box w-50 h-50'; //i%6 < 4 ? 'article box w-25 h-50' : 'article box w-50 h-50';
      var heading = document.createElement("h3");
      heading.className = "heading";
      heading.appendChild(document.createTextNode(entry.title));
      //console.log(entry)
      if (i == 0) {
            var cs = document.getElementById('cover-story');
            cs.innerHTML = "<span>" + "Cover Story" + "</span>" + entry.title.substr(0,80);
            if (entry.title.length > 80) cs.innerHTML += "...";
      }

      var description = document.createElement("p");
      description.className = "description";
      var htmlcontent = $("<div>" + entry.content + "</div>");
      var imgtag = htmlcontent.find("img");
      if (imgtag) imgtag.addClass("master");
      description.innerHTML = htmlcontent.html();
      var images = $("<div>" + entry.content + "</div>").find("img");
      if (images && images.length) {
      	var image =images[0];
      	image.className = "preview_w w-50"; //i%6 < 4 ? "preview_h h-50" : "preview_h h-50";
      }
      var text = htmlcontent.text();
      var text_p = document.createElement("p");
      text_p.appendChild(document.createTextNode(text));

      var snapshot = document.createElement("div");
      snapshot.className = "snapshot";//i%6 < 4 ? "snapshot w-25 h-50" : "snapshot w-50 h-50";
      if (image) snapshot.appendChild(image);
      if (text_p) snapshot.appendChild(text_p);

      //image.className = "preview";
      //description.appendChild(document.createTextNode(entry.content));
      var timestamp = document.createElement("p");
      timestamp.className = "ts";
      var time = moment(entry.publishedDate);
      var timestring = "";
      if (entry.author && entry.author.length > 1) {
      	timestring += "By " + entry.author;
      }
      if (time) {
        if (timestring.length) timestring += ', ';
      	timestring += time.fromNow();
      }
      var reference = document.createElement("a");
      reference.href = entry.link;
      reference.setAttribute("target","_blank");
      reference.target = "_blank";
      reference.appendChild(document.createTextNode("Source"));
      timestamp.appendChild(document.createTextNode(timestring));
      div.appendChild(heading);
      div.appendChild(timestamp);

      div.appendChild(snapshot);
      div.appendChild(description);
      div.appendChild(reference);

      var id = "feed" + Math.floor(i/4);
      var container = document.getElementById(id);
      container.appendChild(div);
};


var add_back_page = function () {
  var app = document.getElementById("flip");
  var back = document.createElement("div");
  back.id = "back-page";
  back.className = "f-page f-cover-back";
  var ad = document.createElement("img");
  ad.id = "codrops-ad-wrapper";
  ad.className = "preview_w w-100"

  ad.src = "images/end.jpg";
  var home = document.createElement("a");
  home.href = "index.html"
  home.setAttribute("href","index.html");
  home.appendChild(document.createTextNode('Go to home page'));
  back.appendChild(ad);
  back.appendChild(home);
  app.appendChild(back);
};
var set_logo = function () {
      var logo = document.getElementById("logo");
      logo.appendChild(document.createTextNode(channel));
      var a = document.createElement("a");
      a.className = "f-ref";
      a.href = "javascript:void(0)";
      a.onclick = function () {location.reload()};
      a.setAttribute('onclick', 'location.reload();');
      a.appendChild(document.createTextNode("Not this feed? Click to reload"));
      logo.appendChild(a);
     // console.log(logo);
} 
var load_feeds = function (feedname, callback) {
      channel = feedname;
      var feed = new google.feeds.Feed(url[channel]);
      feed.setNumEntries(20);
      feed.load(function(result) {
      if (!result.error) {
          var container = document.getElementById("feed");
          //console.log(result.feed.entries.length);
          set_logo();
          for (var i = 0; i < result.feed.entries.length; i++) {
            if (i%4 == 0) {
                  add_page(i);
            }
            handle_article(result, i);
            }
          add_back_page();
          
            /*--------------------------*/
            callback();
            /*----------------------------------*/
            }
      });

}

function initialize () {
      var f = prompt("Which feed would you like to read? options: quora, lifehacker, techcrunch, hsblog, codeofhonor or myblog. Loads news by default!");
      var channel = null;
      var obj = {
            'myblog': 'Blog',
            'lifehacker': 'Lifehacker',
            'porn': 'Porn',
            'techcrunch': 'TechCrunch',
            'quora': 'Quora',
            'news' : 'News',
            'hsblog' : 'Hsblog',
            'codeofhonor': 'codeofhonor'
      }
      if (!f) channel = 'News';
      else channel = obj[f.toLowerCase()] ? obj[f.toLowerCase()] : 'News';
      load_feeds(channel, function () {
            load_modernizer();
      });
}

var data_store = {
  'Me': ['Work','Education','Contact','Projects'],
  'Work': [
  'Sourcebits','Pivotal Security','Technologies known'],
  'Education': [
  'Vellore Institute of Technology, India', 'MOOC'],
  'Sourcebits': [
  'Batzu'],
  'Pivotal Security': [
  'SIEM product prototype'],
  'Contact' : ['Name','Location','Mail','Twitter','Phone'],
  'Name': ['Mayank Gupta'],
  'Mayank Gupta': ['Me'],
  'Location': ['India'],
  'Mail': ['iammayankg@gmail.com'],
  'Twitter': ['@iammayankg'],
  'Phone': ['+919739495041'],
  'Technologies known': ['NodeJS','SOLR','EC2','CUDA','CLIPS','MongoDB'],
  'Projects': ['DuckDuckGo Goodie', 'RSA on CUDA'],
  'MOOC': ['EdX : CS188.1']
};

var description = {
  'Work' : 'I prefer working with smart people.',
  'Sourcebits' : 'I have been <a href="http://www.sourcebits.com/" target="_blank">here</a> since July 2012. Mostly my work involves designing, developing and maintaining APIs for mobile devices. Apart from this I also handle the interactions with clients. You can read the blog I wrote during my training <a href="http://traineediarysb.blogspot.in/" target="_blank">here.</a>',
  'Batzu' : 'Handling the complete backend for an IPad Media Aggregator for viewing feeds from social and RSS channels with search capabilities and flexible content filters. Web scraping, NodeJS, EC2, SOLR, MongoDB.',
  'Me' : 'Hi, This is Mayank. Click on the nodes to explore further.',
  'Pivotal Security' : 'Worked <a href="http://www.pivotalsecurity.com/" target="_blank">here</a> as a work from home intern between April-June 2011 and again in Dec 2011 - May 2012. My work involved rapidly implementing design prototype related to the research on a new generation SIEM product.',
  'SIEM product prototype' : 'Implemented  different prototypes of a Security Integeration and Event management product. C, C++, POSIX threads, CLIPS, MongoDB.',
  'Vellore Institute of Technology, India' : 'Batch of 2012. Graduated with a B.Tech in Information Technology with a CGPA of 8.77/10.',
  'Education' : 'Still learning.',
  'MOOC' : 'I dabble around MOOC taking courses from a wide range of disciplines from Computer Science to philosophy.',
  'Contact' : 'Most of my contact information. You can also mail me any feedbacks/suggestions or post them <a href="http://whatiswrongwith.me/mayank_gupta" target="_blank">here</a>',
  'CUDA': '<a href="http://www.nvidia.in/object/cuda_home_new_in.html" target="_blank">CUDA homepage</a>',
  'NodeJS': '<a href="http://nodejs.org/" target="_blank">NodeJS homepage</a>',
  'SOLR': '<a href="http://lucene.apache.org/solr/" target="_blank">SOLR homepage</a>',
  'CLIPS': '<a href="http://clipsrules.sourceforge.net/" target="_blank">CLIPS homepage</a>',
  'MongoDB': '<a href="http://www.mongodb.org/" target="_blank">MongoDB homepage</a>',
  'EC2' : '<a href="http://aws.amazon.com/ec2/" target="_blank">EC2 homepage</a>',
  'Technologies known': 'Some technologies I have worked on.',
  'Projects': 'Some of my personal projects.',
  'DuckDuckGo Goodie' : 'Wrote a Instant result plugin which shows a random "Chuck Norris" joke whenever searched for "Chuck Norris joke/s fact/s. You can try it by clicking <a href="https://duckduckgo.com/?q=chuck+norris+jokes&kp=-1" target="_blank">here</a>. Please turn safe search off or use "!safeoff" in your query. The code can be found <a href="https://github.com/mr-mayank-gupta/zeroclickinfo-spice" target="_blank">here.</a>',
  'RSA on CUDA' : 'Implemented the basic encryption/description of numbers using RSA on CUDA as a mini project in fall 2011. Used <a href="http://en.wikipedia.org/wiki/Exponentiation_by_squaring#Further_applications" target="_blank">exponential squaring</a> to calculate modulo. The link to the repo <a href="https://bitbucket.org/mayank_gupta/rsa-simulation-on-cuda" target="_blank">RSA simulation on CUDA</a>.',
  'EdX : CS188.1' : 'Completed the course on Artificial Intelligence by UCB with distinction. The repo <a href="https://bitbucket.org/mayank_gupta/artificial-intelligence-cs188" target="_blank">CS188</a> contains the program written for the assignments.'
};

(function($){

  var Renderer = function(canvas){
    var canvas = $('canvas').get(0)
    var ctx = canvas.getContext("2d");
    var particleSystem

    var that = {
      init:function(system){
        //
        // the particle system will call the init function once, right before the
        // first frame is to be drawn. it's a good place to set up the canvas and
        // to pass the canvas size to the particle system
        //
        // save a reference to the particle system for use in the .redraw() loop
        particleSystem = system

        // inform the system of the screen dimensions so it can map coords for us.
        // if the canvas is ever resized, screenSize should be called again with
        // the new dimensions
        particleSystem.screenSize(canvas.width, canvas.height) 
        particleSystem.screenPadding(80) // leave an extra 80px of whitespace per side
        
        // set up some event handlers to allow for node-dragging
        that.initMouseHandling()
      },
      
      redraw:function(){
        // 
        // redraw will be called repeatedly during the run whenever the node positions
        // change. the new positions for the nodes can be accessed by looking at the
        // .p attribute of a given node. however the p.x & p.y values are in the coordinates
        // of the particle system rather than the screen. you can either map them to
        // the screen yourself, or use the convenience iterators .eachNode (and .eachEdge)
        // which allow you to step through the actual node objects but also pass an
        // x,y point in the screen's coordinate system
        // 
        ctx.fillStyle = "white"
        ctx.fillRect(0,0, canvas.width, canvas.height)
        
        particleSystem.eachEdge(function(edge, pt1, pt2){
          // edge: {source:Node, target:Node, length:#, data:{}}
          // pt1:  {x:#, y:#}  source position in screen coords
          // pt2:  {x:#, y:#}  target position in screen coords

          // draw a line from pt1 to pt2
          ctx.strokeStyle = "rgba(0,0,0, .333)"
          ctx.lineWidth = 1
          ctx.beginPath()
          ctx.moveTo(pt1.x, pt1.y)
          ctx.lineTo(pt2.x, pt2.y)
          ctx.stroke()
        })

        particleSystem.eachNode(function(node, pt){
          // node: {mass:#, p:{x,y}, name:"", data:{}}
          // pt:   {x:#, y:#}  node position in screen coords

          // draw a rectangle centered at pt
          var w = ctx.measureText(node.data.label||"").width + 6
          var label = node.name
          if (!(label||"").match(/^[ \t]*$/)){
            pt.x = Math.floor(pt.x)
            pt.y = Math.floor(pt.y)
          }else{
            label = null
          }
          
          // clear any edges below the text label
          // ctx.fillStyle = 'rgba(255,255,255,.6)'
          // ctx.fillRect(pt.x-w/2, pt.y-7, w,14)

          ctx.clearRect(pt.x-w/2, pt.y-7, w,14)

          // draw the text
          if (label){
            ctx.font = "bold 11px Arial"
            ctx.textAlign = "center"
            
            // if (node.data.region) ctx.fillStyle = palette[node.data.region]
            // else ctx.fillStyle = "#888888"
            ctx.fillStyle = "#888888"

            // ctx.fillText(label||"", pt.x, pt.y+4)
            ctx.fillText(label||"", pt.x, pt.y+4)
          }
        })
      },
      
      initMouseHandling:function(){
        // no-nonsense drag and drop (thanks springy.js)
        var dragged = null;

        // set up a handler object that will initially listen for mousedowns then
        // for moves and mouseups while dragging
        var handler = {
          clicked:function(e){
            var pos = $(canvas).offset();
            _mouseP = arbor.Point(e.pageX-pos.left, e.pageY-pos.top)
            dragged = particleSystem.nearest(_mouseP);

            if (dragged && dragged.node !== null){
              // while we're dragging, don't let physics move the node
              dragged.node.fixed = true;
              fillNode(dragged.node);
            }

            $(canvas).bind('mousemove', handler.dragged)
            $(window).bind('mouseup', handler.dropped)

            return false
          },
          dragged:function(e){
            var pos = $(canvas).offset();
            var s = arbor.Point(e.pageX-pos.left, e.pageY-pos.top)

            if (dragged && dragged.node !== null){
              var p = particleSystem.fromScreen(s)
              dragged.node.p = p
            }

            return false
          },

          dropped:function(e){
            if (dragged===null || dragged.node===undefined) return
            if (dragged.node !== null) dragged.node.fixed = false
            dragged.node.tempMass = 1000
            dragged = null
            $(canvas).unbind('mousemove', handler.dragged)
            $(window).unbind('mouseup', handler.dropped)
            _mouseP = null
            return false
          }
        }
        
        // start listening
        $(canvas).mousedown(handler.clicked);

      },
      
    }
    return that
  }
  function removeNode(node) {
    var edges = sys.getEdgesFrom(node);
    for (var i=0; i < edges.length; i++) {
      if (edges[i].target.name == 'Me') {
        sys.pruneEdge(edges[i]);
      }
      else {
        removeNode(edges[i].target)
        sys.pruneNode(edges[i].target);
      }
    }
    node.data.filled = false;
  }
  function fillNode(node) {
    var label = node.name;
    if (description[label]) {
      $('#summaryTitle').html(label);
      $('#summaryText').html(description[label]);
    }

    if (! node.data.filled && nodeCount < nodeLimit) {
        var label = node.name;
        var data = data_store[label];
        if (data) {
          for (var i=0 ; i<data.length; i++) {
            sys.addEdge(node,sys.addNode(data[i], {filled:true}));
          }
        }/*
        if (description[label]) {
          $('#summaryTitle').html(label);
          $('#summaryText').html(description[label]);
        }*/
        node.data.filled = true;
    }
    else if (node.name != 'Me') {
      var label = node.name;
      /*  if (description[label]) {
          $('#summaryTitle').html(label);
          $('#summaryText').html(description[label]);
        }*/
      removeNode(node);
    }
  }
   var sys = arbor.ParticleSystem(1000, 600, 0.5); // create the system with sensible repulsion/stiffness/friction
    var nodeCount = 1;
    var nodeLimit = 1000;
  $(document).ready(function(){
    sys.renderer = Renderer("#explorer") // our newly created renderer will have its .init() method called shortly by sys...
sys.parameters({gravity:true}) // use center-gravity to make the graph settle nicely (ymmv)
 
    // add some nodes to the graph and watch it go...
    
    var me = sys.addNode('Me', {filled:true});
    
    var work = sys.addNode('Work', {filled:true});
    var education = sys.addNode('Education', {filled:true});
    var contact = sys.addNode('Contact', {filled:true});
    var projects = sys.addNode('Projects', {filled:true});

    sys.addEdge(me, work);
    sys.addEdge(me, education);
    sys.addEdge(me, contact);
    sys.addEdge(me, projects);

    $('#summaryTitle').html('Me');
    $('#summaryText').html(description['Me']);
    $.pnotify({
      text: 'Click/Double click on a node to expand or collapse.',
      type: 'info'
    });
    $.pnotify({
      text: 'Description of a node is available below the graph.',
      type: 'info'
    });
  })

})(this.jQuery)
var message = [
[1,1,1,1,1,0,0,0,1,0,0,0,1,0,0,0,1,0,1,1,1,1,0,0,0,0,0,0,0,0],
[1,0,0,0,0,0,0,1,0,1,0,0,1,1,0,1,1,0,1,0,0,0,0,0,0,0,0,0,0,0],
[1,0,1,1,1,0,1,1,1,1,1,0,1,0,1,0,1,0,1,1,1,0,0,0,0,0,0,0,0,0],
[1,0,0,0,1,0,1,0,0,0,1,0,1,0,0,0,1,0,1,0,0,0,0,0,0,0,0,0,0,0],
[1,1,1,1,1,0,1,0,0,0,1,0,1,0,0,0,1,0,1,1,1,1,0,0,0,0,0,0,0,0],
[0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0],
[1,1,1,1,0,1,1,1,1,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0],
[1,0,0,1,0,1,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0],
[1,0,0,1,0,1,1,1,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0],
[1,0,0,1,0,1,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0],
[1,1,1,1,0,1,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0],
[0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0],
[1,0,0,0,0,1,0,1,1,1,0,1,1,1,1,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0],
[1,0,0,0,0,1,0,1,0,0,0,1,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0],
[1,0,0,0,0,1,0,1,1,1,0,1,1,1,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0],
[1,0,0,0,0,1,0,1,0,0,0,1,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0],
[1,1,1,1,0,1,0,1,0,0,0,1,1,1,1,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0],
[0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0],
[0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0],
[0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0]
];

var user_alerts = [
'Welcome to the simulation!',
'Check out the controls tabs to adjust/control the simulation.',
'You can drag the mouse over the canvas to bring the cells to life.',
'Hitting next moves the simulation to the next state and freezes the simulation'];

function Game () {
	this.width = 600;
	this.height = 400;
	this.block_size = 20;
	this.y = this.height/this.block_size;
	this.x = this.width/this.block_size;
	this.grid = message;
};

Game.prototype.clear = function () {
	for (var i=0; i < this.y; i++) {
		for (var j=0; j <this.x; j++) {
			this.grid[i][j] = 0;
		}
	}
};

Game.prototype.beehive = function () {
	this.clear();
	this.grid[0][1] = 1;
	this.grid[0][2] = 1;
	this.grid[1][0] = 1;
	this.grid[1][3] = 1;
	this.grid[2][1] = 1;
	this.grid[2][2] = 1;
};

Game.prototype.boat = function () {
	this.clear();
	this.grid[0][1] = this.grid[0][2] = this.grid[1][1] = this.grid[1][3] = this.grid[2][2] = 1;
};

Game.prototype.glider = function () {
	this.clear();
	this.grid[0][2] = 1;
	this.grid[1][0] = 1;
	this.grid[1][2] = 1;
	this.grid[2][1] = 1;
	this.grid[2][2] = 1;
};

Game.prototype.blinker = function () {
	this.clear();
	this.grid[1][0] = 1;
	this.grid[1][1] = 1;
	this.grid[1][2] = 1;
};

Game.prototype.toad = function () {
	this.clear();
	this.grid[1][1] = this.grid[1][2] = this.grid[1][3] = this.grid[2][0] = this.grid[2][1] = this.grid[2][2] =1;
}
Game.prototype.reset = function () {
	this.grid = message;
};

Game.prototype.set_alive = function (i,j) {
	this.grid[i][j] = 1;
};

Game.prototype.display_grid = function () {
	var x_off = 0;
	var y_off = 0;
	for (var i = 0; i < this.y; i++) {
		for (var j = 0; j < this.x; j++) {
			y_off = i*this.block_size;
			x_off = j*this.block_size;
			context.fillStyle = this.grid[i][j] == 1 ? 'green' : 'grey';
			context.fillRect(x_off, y_off, this.block_size, this.block_size);
		}
	}
};

Game.prototype.active_neighbours = function (i, j) {
	var neighbours = 0;
	if (i > 0) {
		neighbours = this.grid[i-1][j];
		if (j > 0) neighbours += this.grid[i-1][j-1];
		if (j < this.x-1) neighbours += this.grid[i-1][j+1];
	}
	if (i < this.y-1) {
		neighbours += this.grid[i+1][j];
		if (j > 0) neighbours += this.grid[i+1][j-1];
		if (j < this.x-1) neighbours += this.grid[i+1][j+1];		
	}
	if (j > 0) neighbours += this.grid[i][j-1];
	if (j < this.x-1) neighbours += this.grid[i][j+1];
	return neighbours;
};

Game.prototype.next_tick = function () {
	var new_grid = new Array(this.y);

	for (var i = 0; i < this.y; i++) {
		new_grid[i] = new Array(this.x);
		for (var j = 0; j < this.x; j++) {
			var neighbours = this.active_neighbours(i,j)
			switch(this.grid[i][j]) {
				case 0: 
				new_grid[i][j] = neighbours == 3 ? 1 : 0;
				break;
				case 1:
				new_grid[i][j] = (neighbours == 2 || neighbours == 3) ? 1 : 0;
				break;
			}
		}
	}
	this.grid = new_grid;
};

var canvas = document.getElementById("game"),
context = canvas.getContext("2d");
var game = new Game();
var mouse_pressed = false;
var interval_id = null;

function on_click(e) {
	var mouseX, mouseY;

    game.set_alive(Math.ceil(e.pageY/game.block_size), Math.ceil(e.pageX/game.block_size));
    game.display_grid();
};

function mouse_up(e) {
	mouse_pressed = false;
};

function mouse_down(e) {
	mouse_pressed = true;
};

function mouse_move(e)
{
    var mouseX, mouseY;

    if(e.offsetX) {
        mouseX = e.offsetX;
        mouseY = e.offsetY;
    }
    else if(e.layerX) {
        mouseX = e.layerX;
        mouseY = e.layerY;
    }

    if (mouse_pressed) {
    	game.set_alive(Math.ceil(mouseY/game.block_size), Math.ceil(mouseX/game.block_size));
    	game.display_grid();
    }
};

var refresh = function () {
	game.next_tick();
	game.display_grid();
};

var start_loop = function() {
	canvas.addEventListener("click", on_click, false);
	canvas.addEventListener("mousedown", mouse_down, false);
	canvas.addEventListener("mouseup", mouse_up, false);
	canvas.addEventListener("mousemove", mouse_move, false);
	interval_id = setInterval(refresh, 500);
};

var pause_simulation = function () {
	if (interval_id) {
		window.clearInterval(interval_id);
		interval_id = null;
	}
};

var play_simulation = function () {
	if (!interval_id) {
		interval_id = setInterval(refresh, 500);
	}
};

var clear_canvas = function () {
	game.clear();
	game.display_grid();
};

var reset_canvas = function () {
	game.reset();
	game.display_grid();
};

var set_shape = function (shape) {
	pause_simulation();
	switch(shape) {
		case 'beehive':
		game.beehive();
		break;
		case 'boat':
		game.boat();
		break;
		case 'toad':
		game.toad();
		break;
		case 'glider':
		game.glider();
		break;
		case 'blinker':
		game.blinker();
		break;
	}
	game.display_grid();
	setTimeout(start_loop, 3000);
}

var set_beehive = function () {
	set_shape('beehive');
};
var set_boat = function () {
	set_shape('boat');
};
var set_toad = function () {
	set_shape('toad');
};
var set_glider = function() {
	set_shape('glider');
};

var set_blinker = function () {
	set_shape('blinker');
};

var next_move = function () {
	pause_simulation();
	game.next_tick();
	game.display_grid();
};
var msg_index = 0;
var notification_id = null;
var send_notifications = function () {
	if (msg_index == user_alerts.length) {
		window.clearInterval(notification_id);
		return;
	}
	$.pnotify({
		text: user_alerts[msg_index],
		type: 'info'
	});
	msg_index = (msg_index+1);
};

var describe_oscillator = function () {
	$.pnotify({
		text: 'Oscillators are repeating patterns.',
		type: 'info'
	});
};
var describe_still = function () {
	$.pnotify({
		text: 'Still patterns remain stable.',
		type: 'info'
	});	
};
var describe_spaceships = function () {
	$.pnotify({
		text: 'Spaceship patterns move through the board.',
		type: 'info'
	});		
};
var init = function ()  {
	game.display_grid();
	setTimeout(start_loop, 5000);
	notification_id = setInterval(send_notifications, 3000);
};
